import java.util.*;
import java.io.*;



public class Graph {

	public static int[][] adj;
	public static int[] info;
	public static boolean[] visited;
	public int[] maxways;
	public static int ways=0;
	
	
	public static boolean check(int p, int k){       
		try{
			if (p == k) return true;
			for(int i = 0; i<adj.length; i++){
				if(adj[i][p]==1 && check(i,k)==true) return true;
			}
		}
		catch(StackOverflowError e) {return false;}
		return false;
		
	}
	
	public static void printv() throws IOException{                //Pary smezhnyh vershin
		Scanner reader = new Scanner(new FileReader("E:/matrx.txt"));
		PrintWriter writer = new PrintWriter("E:/output1.txt");
		int v=reader.nextInt();
		for(int i=0; i<v; i++){
			for(int j=0; j<v; j++){
				if(reader.nextInt()==1){
					writer.println(i+" "+j);
				}
			}
		}
		writer.close();
		reader.close();
	}
	
	public Graph(int n){
		
		 adj = new int[n][n];
		 maxways = new int[n];
		 info = new int[n];
		 maxways[0]=-1;
		for(int i=1; i<n; i++){
			maxways[i]=-1;
			info[i] = 0;
			for(int j=0; j<n; j++){
				adj[i][j]=0;
			}
		}
	}
	
	public int longestway(int current){    
		if (current==0) return 0;
		int max = 0;
		for(int i=0; i<adj.length; i++){
			if(adj[i][current]==1 && longestway(i)+1>max) max=longestway(i)+1;
		}
		return max;
	}
	
	public static int ways(int dest){         //korotky put'
		Queue<Integer> q = new LinkedList<Integer>();
		int[] dist = new int[adj.length];
		for(int i =0; i< adj.length;i++){
			dist[i]= -1;
		}
		dist[0]=0;
		q.add(0);
		int a;
		while((a = q.remove() ) != dest){
			int d = dist[a]+1;
			for(int i=0; i<=dest; i++){
				if(adj[a][i]==1&&dist[i]==-1){
					dist[i]=d;
					q.add(i);
				}	
			}
		}
		return dist[dest];
	}
	
	public void print() throws IOException {
		PrintWriter writer = new PrintWriter("E:/matrix.txt");
		for(int i=0; i<adj.length; i++){
			for(int j=0; j<adj.length; j++){
				writer.print(adj[i][j]);
			}
			writer.println();
		}
		writer.close();
		
	}
	
	public boolean diffways(int dest){      //task3
		Queue <Integer> q = new LinkedList <Integer>();
		int count = 0;
		q.add(0);
		while(!q.isEmpty()){
			for(int i=1; i<adj.length; i++){
				if(adj[q.element()][i]==1&check(dest,i)) {
					q.add(i);
					count++;
				}
				if(i==adj.length-1) q.remove();
			}
			if(count==2) return true;
			else {
				count = 0;
			}
		}
		return false;
	}
	
	public void bfs(){
		visited = new boolean [adj.length];
		for(int i=1; i<adj.length; i++){
			visited[i]=false;
		}
		visited[0]=true;
		Queue <Integer> q = new LinkedList <Integer> ();
		q.add(0);
		while(!q.isEmpty()){
			for(int i=0; i<adj.length; i++){
				if(adj[q.element()][i]==1&&visited[i]==false) {
					q.add(i);
					visited[i]=true;
				}
							
			}
			q.remove();
			
			
		}
		
		
	}
	
	
	
	public static boolean dfs(int a){ 
		info[a]=1;
		for(int i=0; i<adj.length; i++){
			if(adj[a][i]==1 && info[i]==1){
				return false;
			}
			if(adj[a][i]==1){
				return dfs(i);
				
			}
		}
		info[a]=2;
		return true;
	}
	
	public static boolean isaff(){           //cycles?
		for(int i=0; i<adj.length; i++){
			for(int j=0; j<adj.length; j++){
				info[j] = 0;
			}
			if(!dfs(i)){
				return false;
			}
		}
		return true;
	}
	
	/*public boolean isaff(){        //cycles?
		Queue <Integer> q = new LinkedList <Integer> ();
		int[][] visited = new int[adj.length][adj.length];
		for(int i=0; i<adj.length; i++){
			for(int j=0; j<adj.length; j++){
				visited[i][j]=0;
			}	
		}
		q.add(0);
		visited[0][0]=1;
		while(!q.isEmpty()){
			for(int i=0; i<adj.length; i++){
				if(adj[q.element()][i]==1 && visited[q.element()][i]==) {
					visited[q.element()][i]=true;
					q.add(i);
				}
			}
			q.remove();
		}
 		return true;
	}*/
	
	public int razv(){        //nearest fork
		double minway = Double.POSITIVE_INFINITY;
		int min=adj.length;
		int count = 0;
		try{
			for(int i=0; i<adj.length; i++){
				for(int j=0; j<adj.length; j++){
					if(adj[i][j]==1 && ways(i)<minway) count++;
					if(count==2 && ways(j)<minway){
						minway = ways(i);
						min = i; 
					}
				}
				count = 0;
			}
		}
		catch(NoSuchElementException e) {return -1;}	
		if(min==adj.length) return -1;
		else return min;
	}

}
