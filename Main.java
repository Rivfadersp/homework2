import java.util.Scanner;
import java.io.*;


public class Main {

	public static void main(String[] args)throws IOException  {
		// TODO Auto-generated method stub
		
		Scanner s = new Scanner(System.in);
		
	
		

		
		Scanner reader = new Scanner(new FileReader("E:/test.txt"));
		
		int n = reader.nextInt();
		
		Graph g = new Graph(n);
		int v1;
		
		while((v1=reader.nextInt())!=-1){
			int v2=reader.nextInt();
			g.adj[v1][v2]=1;
		}
		
		reader.close();
		
		
		PrintWriter writer = new PrintWriter("E:/output.txt");
		
		if(!g.isaff()) 
			writer.println("Cycle detected");
		else{
			System.out.println("Two different ways test, enter the point");
			int p = s.nextInt();
			writer.println("Two different ways to " + p +" - " + g.diffways(p));
			System.out.println("The shortest and the longest ways, enter the point");
			int v = s.nextInt();
			writer.println("The shortest way to " + v +" is " + g.ways(v));
			writer.println("The longest way to " + v +" is " + g.longestway(v));
			if(g.razv()==-1) writer.println("Fork wasn't detected");
			else writer.println("Fork at " + g.razv());
		}
		//g.printv();
		//for(int i=0; i<g.visited.length; i++){
			//writer.println(g.visited[i]);
		//}
		
		
		writer.println("No cycles - " + g.isaff());
		
		writer.close();

	}
}


